# Safari extension "QRify"

This project is an extension for the Safari web browser that adds a toolbar button to generate a QR code for the current
website's URL; this QR code can be used to easily transfer websites from your desktop computer to your mobile device.

For generation of the QR code, a JavaScript library named QRious is used (https://github.com/neocotic/qrious).
